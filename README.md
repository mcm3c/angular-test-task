# test-task

This is an old small demo for demonstrating Angular.js knowledge. That was requested to be done in 2016 as a test task before an interview.

Environment:

- Yeoman - Angular Yeoman generator for automation the extending of the application
- Grunt as a task manager
- Bower and npm for controlling dependencies
- Git (via Bitbucket) as a VCS
- Heroku as PaaS, added as a submodule to the dist folder


Deployed at:

https://upwork-angular-test-app.herokuapp.com/


--

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.15.1.

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.
