'use strict';

describe('Controller: DirectorCtrl', function () {

  // load the controller's module
  beforeEach(module('testTaskApp'));

  var DirectorCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    DirectorCtrl = $controller('DirectorCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(DirectorCtrl.awesomeThings.length).toBe(3);
  });
});
