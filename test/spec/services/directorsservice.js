'use strict';

describe('Service: DirectorsService', function () {

  // load the service's module
  beforeEach(module('testTaskApp'));

  // instantiate service
  var DirectorsService;
  beforeEach(inject(function (_DirectorsService_) {
    DirectorsService = _DirectorsService_;
  }));

  it('should do something', function () {
    expect(!!DirectorsService).toBe(true);
  });

});
