'use strict';

/**
 * @ngdoc function
 * @name testTaskApp.controller:DirectorCtrl
 * @description
 * # DirectorCtrl
 * Controller of the testTaskApp
 */
angular.module('testTaskApp')
  .controller('DirectorCtrl', function($scope, $routeParams, DirectorsService, $location) {
    $scope.directorDob = $routeParams[1];
    if (!$scope.directorDob) {
      $location.path('/');
      return;
    }
    $scope.directorData = {};

    /*
    because the ids are not provided and since the directors' data is explicitly cached
    (cache: true in the directors resource options and in fact it's just a json file) we could
    use array index as an id but int this case date of birth seems more reliable
    */
    DirectorsService.getDirectorsList().then(function(directorsList) {
      $scope.directorData = _.find(directorsList, {dob: +$scope.directorDob});
    });
  });
