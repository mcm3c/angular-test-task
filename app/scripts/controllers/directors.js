'use strict';

/**
 * @ngdoc function
 * @name testTaskApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the testTaskApp
 */
angular.module('testTaskApp')
  .controller('DirectorsCtrl', function($scope, DirectorsService, $location) {
    $scope.directorsList = [];
    DirectorsService.getDirectorsList().then(function(directorsList) {
      $scope.directorsList = directorsList;
    });

    $scope.showDetails = function(directorIndex) {
      console.log('details', arguments);
      $location.path('/director/' + directorIndex);
    };
  });
