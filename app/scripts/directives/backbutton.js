'use strict';

/**
 * @ngdoc directive
 * @name testTaskApp.directive:backButton
 * @description
 * # backButton
 */
angular.module('testTaskApp')
  .directive('backButton', function () {
    return {
      restrict: 'A',
      link: function postLink(scope, element, attrs) {
        element.on('click', function () {
          history.back();
          scope.$apply();
        });
      }
    };
  });
