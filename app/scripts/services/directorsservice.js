'use strict';

/**
 * This service can contain additional logic related to the director entity
 * @ngdoc service
 * @name testTaskApp.DirectorsService
 * @description
 * # DirectorsService
 * Service in the testTaskApp.
 */
angular.module('testTaskApp')
  .service('DirectorsService', function (appConfig, $resource, $q) {
    var directorsService = this;
    var directorsResource = $resource(appConfig.directorsURL, {
      cache: true
    });

    /**
     * Returns a list of directors
     * @returns Deferred
     */
    directorsService.getDirectorsList = function() {
      var deferredGetDirectorsList = $q(function(resolveGetDirectorsList, rejectGetDirectorsList) {
        directorsResource.query(function(directorsData) {
          resolveGetDirectorsList(directorsData);
        });
      });

      return deferredGetDirectorsList;
    };
  });
