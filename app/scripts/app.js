'use strict';

/**
 * @ngdoc overview
 * @name testTaskApp
 * @description
 * # testTaskApp
 *
 * Main module of the application.
 */
angular
  .module('testTaskApp', [
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .constant('appConfig', {
    'directorsURL': '/directors.json'
  })
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        redirectTo: '/directors'
      })
      .when('/directors', {
        templateUrl: 'views/directors.html',
        controller: 'DirectorsCtrl',
        controllerAs: 'directors'
      })
      .when('/director/:1', {
        templateUrl: 'views/director.html',
        controller: 'DirectorCtrl',
        controllerAs: 'director'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
